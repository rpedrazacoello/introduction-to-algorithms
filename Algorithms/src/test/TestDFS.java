package test;

import algorithms.explore_graphs.unweighted_edges.DepthFirstSearch;
import data_structures.graphs.DepthFirstSearch.DFSNode;
import data_structures.graphs.Edge;

public class TestDFS {
    public static void main (String [] args){
        DepthFirstSearch depthFirstSearch = new DepthFirstSearch(true);

        DFSNode A = new DFSNode("AAA");
        DFSNode B = new DFSNode("BBB");
        DFSNode C = new DFSNode("CCC");
        DFSNode D = new DFSNode("DDD");
        DFSNode E = new DFSNode("EEE");
        DFSNode F = new DFSNode("FFF");
        DFSNode G = new DFSNode("GGG");
        DFSNode H = new DFSNode("HHH");
        DFSNode I = new DFSNode("III");
        DFSNode J = new DFSNode("JJJ");

        Edge edge1 = new Edge(A, C);
        Edge edge2 = new Edge(B, C);
        Edge edge3 = new Edge(B, D);
        Edge edge4 = new Edge(C, D);
        Edge edge5 = new Edge(D, E);
        Edge edge6 = new Edge(E, F);
        Edge edge7 = new Edge(E, G);
        Edge edge8 = new Edge(F, G);
        Edge edge9 = new Edge(G, J);
        Edge edge10 = new Edge(H, I);
        Edge edge11 = new Edge(H, J);
        Edge edge12 = new Edge(I, J);

        depthFirstSearch.addEdge(edge1);
        depthFirstSearch.addEdge(edge2);
        depthFirstSearch.addEdge(edge3);
        depthFirstSearch.addEdge(edge4);
        depthFirstSearch.addEdge(edge5);
        depthFirstSearch.addEdge(edge6);
        depthFirstSearch.addEdge(edge7);
        depthFirstSearch.addEdge(edge8);
        depthFirstSearch.addEdge(edge9);
        depthFirstSearch.addEdge(edge10);
        depthFirstSearch.addEdge(edge11);
        depthFirstSearch.addEdge(edge12);

        depthFirstSearch.addNode(A);
        depthFirstSearch.addNode(B);
        depthFirstSearch.addNode(C);
        depthFirstSearch.addNode(D);
        depthFirstSearch.addNode(E);
        depthFirstSearch.addNode(F);
        depthFirstSearch.addNode(G);
        depthFirstSearch.addNode(H);
        depthFirstSearch.addNode(I);
        depthFirstSearch.addNode(J);

        depthFirstSearch.run();
        depthFirstSearch.printRoots();
        depthFirstSearch.printTopologicalSort();
        depthFirstSearch.printRouteFromRootToNodeV(J);
    }
}
