package test;

import algorithms.explore_graphs.weighted_edges.BellmanFord;
import data_structures.graphs.WeightGraph.WeightEdge;
import data_structures.graphs.WeightGraph.WeightNode;

public class TestBellmanFord {

    public static void main (String [] args){

        BellmanFord bellmanFord = new BellmanFord();
        WeightNode A = new WeightNode("A");
        WeightNode B = new WeightNode("B");
        WeightNode C = new WeightNode("C");
        WeightNode D = new WeightNode("D");
        WeightNode E = new WeightNode("E");
        WeightNode F = new WeightNode("F");
        WeightNode G = new WeightNode("G");
        WeightNode H = new WeightNode("H");
        WeightNode I = new WeightNode("I");
        WeightNode J = new WeightNode("J");

        WeightEdge edge1 = new WeightEdge(A, C, 1);
        WeightEdge edge2 = new WeightEdge(B, C, 1);
        WeightEdge edge3 = new WeightEdge(B, D, 1);
        WeightEdge edge4 = new WeightEdge(C, D, 5);
        WeightEdge edge5 = new WeightEdge(D, E, 1);
        WeightEdge edge6 = new WeightEdge(E, F, 1);
        WeightEdge edge7 = new WeightEdge(G, E, -3);
        WeightEdge edge8 = new WeightEdge(F, G, 1);
        WeightEdge edge9 = new WeightEdge(G, J, 1);
        WeightEdge edge10 = new WeightEdge(H, I, 1);
        WeightEdge edge11 = new WeightEdge(H, J, 1);
        WeightEdge edge12 = new WeightEdge(I, J, 3);

        bellmanFord.addEdge(edge1);
        bellmanFord.addEdge(edge2);
        bellmanFord.addEdge(edge3);
        bellmanFord.addEdge(edge4);
        bellmanFord.addEdge(edge5);
        bellmanFord.addEdge(edge6);
        bellmanFord.addEdge(edge7);
        bellmanFord.addEdge(edge8);
        bellmanFord.addEdge(edge9);
        bellmanFord.addEdge(edge10);
        bellmanFord.addEdge(edge11);
        bellmanFord.addEdge(edge12);

        bellmanFord.addNode(A);
        bellmanFord.addNode(B);
        bellmanFord.addNode(C);
        bellmanFord.addNode(D);
        bellmanFord.addNode(E);
        bellmanFord.addNode(F);
        bellmanFord.addNode(G);
        bellmanFord.addNode(H);
        bellmanFord.addNode(I);
        bellmanFord.addNode(J);

        bellmanFord.setRootIdentifier(A);
        bellmanFord.run();
        bellmanFord.printRouteFromRootToNodeV(J);
    }
}
