package test;

import algorithms.explore_graphs.unweighted_edges.BreadthFirstSearch;
import data_structures.graphs.Edge;
import data_structures.graphs.Node;

public class TestBFS {

    public static void main (String [] args){
        BreadthFirstSearch BFS = new BreadthFirstSearch(false);
        Node A = new Node("A");
        Node B = new Node("B");
        Node C = new Node("C");
        Node D = new Node("D");
        Node E = new Node("E");
        Node F = new Node("F");
        Node G = new Node("G");
        Node H = new Node("H");
        Node I = new Node("I");
        Node J = new Node("J");

        Edge edge1 = new Edge(A, C);
        Edge edge2 = new Edge(B, C);
        Edge edge3 = new Edge(B, D);
        Edge edge4 = new Edge(C, D);
        Edge edge5 = new Edge(D, E);
        Edge edge6 = new Edge(E, F);
        Edge edge7 = new Edge(E, G);
        Edge edge8 = new Edge(F, G);
        Edge edge9 = new Edge(G, J);
        Edge edge10 = new Edge(H, I);
        Edge edge11 = new Edge(H, J);
        Edge edge12 = new Edge(I, J);

        BFS.addEdge(edge1);
        BFS.addEdge(edge2);
        BFS.addEdge(edge3);
        BFS.addEdge(edge4);
        BFS.addEdge(edge5);
        BFS.addEdge(edge6);
        BFS.addEdge(edge7);
        BFS.addEdge(edge8);
        BFS.addEdge(edge9);
        BFS.addEdge(edge10);
        BFS.addEdge(edge11);
        BFS.addEdge(edge12);

        BFS.addNode(A);
        BFS.addNode(B);
        BFS.addNode(C);
        BFS.addNode(D);
        BFS.addNode(E);
        BFS.addNode(F);
        BFS.addNode(G);
        BFS.addNode(H);
        BFS.addNode(I);
        BFS.addNode(J);

        BFS.setRootIdentifier(A);
        BFS.run();
        BFS.printRouteFromRootToNodeV(J);
    }
}
