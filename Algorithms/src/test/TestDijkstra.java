package test;

import algorithms.explore_graphs.weighted_edges.Dijkstra;
import data_structures.graphs.Node;
import data_structures.graphs.WeightGraph.WeightEdge;
import data_structures.graphs.WeightGraph.WeightNode;

public class TestDijkstra {

    public static void main (String [] args){

        Dijkstra dijkstra = new Dijkstra(true);
        WeightNode A = new WeightNode("A");
        WeightNode B = new WeightNode("B");
        WeightNode C = new WeightNode("C");
        WeightNode D = new WeightNode("D");
        WeightNode E = new WeightNode("E");
        WeightNode F = new WeightNode("F");
        WeightNode G = new WeightNode("G");
        WeightNode H = new WeightNode("H");
        WeightNode I = new WeightNode("I");
        WeightNode J = new WeightNode("J");

        WeightEdge edge1 = new WeightEdge(A, C, 1);
        WeightEdge edge2 = new WeightEdge(B, C, 1);
        WeightEdge edge3 = new WeightEdge(B, D, 1);
        WeightEdge edge4 = new WeightEdge(C, D, 5);
        WeightEdge edge5 = new WeightEdge(D, E, 1);
        WeightEdge edge6 = new WeightEdge(E, F, 1);
        WeightEdge edge7 = new WeightEdge(E, G, 3);
        WeightEdge edge8 = new WeightEdge(F, G, 1);
        WeightEdge edge9 = new WeightEdge(G, J, 1);
        WeightEdge edge10 = new WeightEdge(H, I, 1);
        WeightEdge edge11 = new WeightEdge(H, J, 1);
        WeightEdge edge12 = new WeightEdge(I, J, 3);

        dijkstra.addEdge(edge1);
        dijkstra.addEdge(edge2);
        dijkstra.addEdge(edge3);
        dijkstra.addEdge(edge4);
        dijkstra.addEdge(edge5);
        dijkstra.addEdge(edge6);
        dijkstra.addEdge(edge7);
        dijkstra.addEdge(edge8);
        dijkstra.addEdge(edge9);
        dijkstra.addEdge(edge10);
        dijkstra.addEdge(edge11);
        dijkstra.addEdge(edge12);

        dijkstra.addNode(A);
        dijkstra.addNode(B);
        dijkstra.addNode(C);
        dijkstra.addNode(D);
        dijkstra.addNode(E);
        dijkstra.addNode(F);
        dijkstra.addNode(G);
        dijkstra.addNode(H);
        dijkstra.addNode(I);
        dijkstra.addNode(J);

        dijkstra.setRootIdentifier(A);
        dijkstra.run();
        dijkstra.printRouteFromRootToNodeV(I);
    }
}
