package test;

import algorithms.sorting.InsertionSort;
import algorithms.sorting.MergeSort;

public class TestSorting {

    public static void main (String [] args){

        InsertionSort insertionSort = new InsertionSort(5);
        insertionSort.insert(7);
        insertionSort.insert(4);
        insertionSort.insert(10);
        insertionSort.insert(2);
        insertionSort.insert(1);
        insertionSort.printArray();

        MergeSort mergeSort = new MergeSort();
        int [] a = {1, 5, 4, 8, 9, 2 ,12, 7, 3, 9, 10};
        int [] sorted = mergeSort.sort(a);
        System.out.println("\n\nArreglo ordenado: ");
        for (int x: sorted) {
            System.out.print(x +", ");
        }

    }
}
