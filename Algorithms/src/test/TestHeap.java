package test;

import data_structures.Heap.Heapify.MaxHeapify;
import data_structures.Heap.Heapify.HeapNode;

public class TestHeap {

    public static void main (String [] args){
        MaxHeapify maxHeapify = new MaxHeapify(new HeapNode(1, "1"));
        HeapNode h2 = new HeapNode(2, "2");
        HeapNode h3 = new HeapNode(3, "3");
        HeapNode h4 = new HeapNode(4, "4");
        HeapNode h5 = new HeapNode(5, "5");
        HeapNode h6 = new HeapNode(6, "6");
        HeapNode h7 = new HeapNode(7, "7");
        HeapNode h8 = new HeapNode(8, "8");
        HeapNode h9 = new HeapNode(9, "9");
        HeapNode h10 = new HeapNode(10, "10");
        HeapNode h11 = new HeapNode(11, "11");
        HeapNode h12 = new HeapNode(12, "12");
        HeapNode h13 = new HeapNode(13, "13");
        HeapNode h14 = new HeapNode(14, "14");
        HeapNode h15 = new HeapNode(15, "15");


        maxHeapify.addToHeap(h2);
        maxHeapify.addToHeap(h3);
        maxHeapify.addToHeap(h4);
        maxHeapify.addToHeap(h5);
        maxHeapify.addToHeap(h6);
        maxHeapify.addToHeap(h7);
        maxHeapify.addToHeap(h8);
        maxHeapify.addToHeap(h9);
        maxHeapify.addToHeap(h10);
        maxHeapify.addToHeap(h11);
        maxHeapify.addToHeap(h12);
        maxHeapify.addToHeap(h13);
        maxHeapify.addToHeap(h14);
        maxHeapify.addToHeap(h15);

        h2.setKey(20);
        h15.setKey(11);
        maxHeapify.updateFromHeap(h15);
        maxHeapify.updateFromHeap(h2);
        maxHeapify.removeFromHeap(h11);

        maxHeapify.printOrder();
    }
}
