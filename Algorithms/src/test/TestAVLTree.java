package test;

import data_structures.AVLTree.AVLTree;

import java.util.concurrent.ThreadLocalRandom;

public class TestAVLTree {

    public static void main (String[] args) throws InterruptedException {

        AVLTree tree = new AVLTree(10);
        tree.insertNode(343);
        tree.insertNode(501);
        tree.insertNode(964);
        tree.insertNode(480);
        tree.insertNode(569);
        tree.insertNode(923);
        tree.insertNode(286);
        tree.insertNode(182);
        tree.printTree();
        System.out.println("---------------");
        tree.removeNode(286);
        tree.removeNode(501);
        tree.removeNode(480);
        tree.removeNode(569);
        tree.printTree();
    }
}
