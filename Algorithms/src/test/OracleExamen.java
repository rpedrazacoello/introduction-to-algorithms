package test;

import java.util.Hashtable;

public class OracleExamen {

    public static void main (String args[]){
        Hashtable <String, Integer> record = new Hashtable();
        String string = "abcdbdsdfng";
        int maxCount = Integer.MIN_VALUE;
        int count;
        for(int i=0; i<=string.length(); i++){
            int temp = i;
            count = 0;
            for(int j=i+1; j<=string.length(); j++){
                String sub = string.substring(temp, j);
                if(record.containsKey(sub)){
                    record.clear();
                    break;
                } else {
                    record.put(sub,1);
                    count++;
                    temp++;
                    if(maxCount<count){
                        maxCount=count;
                    }
                }
            }
        }
        System.out.println(maxCount);
    }
}
