package test;

import algorithms.peak_finder.OneDimensionPeakFinder;
import algorithms.peak_finder.TwoDimensionPeakFinder;

public class TestPeakFinder {

    public static void main(String[] args) {

        //Probando PeakFinder de una dimension
        int [] peakTest = {2, 4, 6, 5, 8, 3, 7, 8};
        OneDimensionPeakFinder oneDimensionPeakFinder = new OneDimensionPeakFinder();
        int peakPlace = oneDimensionPeakFinder.findPeak(peakTest);
        System.out.println("Peak Finder de 1 dimension.......................................");
        System.out.println(
               "Lugar en el Array: " +peakPlace
                +"\nValor en el Array: " +peakTest[peakPlace]
        );



        //Probando PeakFinder de dos dimensiones
        int [][] peakTest2 =
                {
                {2, 4, 6, 5, 8, 3, 7, 8},
                {4, 6, 3, 2, 6, 8, 1, 3}
                };
        TwoDimensionPeakFinder twoDimensionPeakFinder = new TwoDimensionPeakFinder();
        int [] peakPlace2 = twoDimensionPeakFinder.peakFinder(peakTest2);
        System.out.println("\n\n Finder de 2 dimensiones.......................................");
        System.out.println("Fila en el Array: " +peakPlace2[0]
                +"\nColumna en el Array: " +peakPlace2[1]
                +"\nValor en el Array: " +peakTest2[peakPlace2[0]][peakPlace2[1]]
        );
    }
}
