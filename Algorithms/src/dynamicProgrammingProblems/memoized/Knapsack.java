package dynamicProgrammingProblems.memoized;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * PROBLEM EXPLANATION:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/lecture-21-dp-iii-parenthesization-edit-distance-knapsack/
 * https://en.wikipedia.org/wiki/Knapsack_problem
 *
 * Given a set of items, each with a weight and a value, determine the number
 * of each item to include in a collection so that the total weight is less than
 * or equal to a given limit and the total value is as large as possible. It derives
 * its name from the problem faced by someone who is constrained by a fixed-size
 * knapsack(backpack) and must fill it with the most valuable items.
 *
 * Knapsack of size S you want to pack
 *      - item i has integer size si & real value vi
 *      - choose subset of items of maximum total value subject to total size ≤ S
 */
public class Knapsack {

    //[i][0]=value of item i             [i][1]=size of item i
    static int [][] value_size= {{5,1},{10,10},{2,2},{4,7},{10,3},{7,3},{5,4},{4,4},{20,10},{20,10}};
    static String [] items2 = {"Knife", "Tent", "Binoculars", "Sleeping-bag", "Water", "Lantern", "Cord", "Cutting Board", "Boots", "Toilet Paper"};
    static Hashtable<String, Integer> optimal;
    static int backpackSize = 30;
    static Hashtable<String, String> parent;
    static Hashtable<String, Integer> selectedItems;
    static final String NO_PARENT = "n/a";

    public static void main (String [] args){
        optimal = new Hashtable<>();
        parent = new Hashtable<>();
        selectedItems = new Hashtable<>();

        int max = maxValue(0, backpackSize);
        System.out.println("Max value: " +max +"\n");
        printResults();
    }

    /**
     * Method that decides if an item is added or not, with the end of maximizing the value
     * of the selected items, with out surpassing the limit (allowedSize)
     * @param start
     * @param allowedSize
     * @return
     */
    private static int maxValue(int start, int allowedSize){
        String indexKey = creteKey(start, allowedSize);

        if(optimal.containsKey(indexKey)){
            return optimal.get(indexKey); //Memoization
        }

        //Base case: There are no more items to select. So the value = 0
        if(start>=items2.length){
            optimal.put(indexKey, 0);
            parent.put(indexKey, NO_PARENT);
            return optimal.get(indexKey);
        }

        for(int i=start; i<items2.length; i++){
            int itemValue = value_size[i][0];
            int itemSize = value_size[i][1];
            int newAllowedSize = allowedSize < itemSize ? 0 : allowedSize-itemSize;
            int valueBuyItem = 0;

            if(allowedSize >= itemSize){
                //Max Value if you buy the next item (i+1) with a certain allowedSize
                valueBuyItem = maxValue(i+1, newAllowedSize) + itemValue;
            }

            //Max Value if you don't buy the next item (i+1) with a certain allowedSize
            int valueNotBuyItem = maxValue(i+1, allowedSize); //Recursion

            if(!optimal.containsKey(indexKey) || optimal.get(indexKey)<valueNotBuyItem || optimal.get(indexKey)<valueBuyItem){
                if(valueBuyItem>valueNotBuyItem){
                    optimal.put(indexKey, valueBuyItem); //Memoization

                    //Parent pointers to print the result
                    String parentKey = creteKey(i+1, newAllowedSize);
                    parent.put(indexKey, parentKey);
                    selectedItems.put(parentKey, i);
                } else {
                    optimal.put(indexKey, valueNotBuyItem); //Memoization

                    //Parent Pointers to print the result
                    String parentKey = creteKey(i+1, allowedSize);
                    parent.put(indexKey, parent.get(parentKey));
                }
            }
        }

        return optimal.get(indexKey);
    }


    /**
     * Prints the results using the parent pointers
     */
    private static void printResults(){
        String rootKey = creteKey(0, backpackSize);

        System.out.println("Selected Items:");
        String p = rootKey;
        if(!selectedItems.containsKey(p)){
            p = parent.get(rootKey);
        }
        while(p!=NO_PARENT){
            System.out.println("Name:" +items2[selectedItems.get(p)]
                    +"  Size:" +value_size[selectedItems.get(p)][1]
                    +"  Value: " +value_size[selectedItems.get(p)][0]);
            p = parent.get(p);
        }
    }

    /**
     * The hashtables keys have the form "i,allowedSize".
     * This because, to maximize the value, the algorithm cares about
     * how much items do you have left to visit and how much space you
     * have left.
     * @param i
     * @param allowedSize
     * @return hashtables keys
     */
    private static String creteKey(int i, int allowedSize){
        return i+","+allowedSize;
    }
}
