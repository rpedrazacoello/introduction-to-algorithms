package dynamicProgrammingProblems.memoized;

import java.util.Scanner;

/**
 * PROBLEM OBTAINED FROM:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/assignments/MIT6_006F11_ps7.pdf
 *
 * You have been given an internship at the extremely profitable and secretive bank HG Fargo. Your
 * immediate supervisor tells you that higher-ups in the bank are very interested in learning from the
 * past. In particular, they want to know how much money they could have made if they had invested
 * optimally.
 *
 * Your supervisor gives you the following data on the prices1 of select stocks in 1991 and in 2011:
 * Company Price in 1991 Price in 2011
 *                                                      START   END
 *                                      Dale, Inc.      $12     $39
 *                                      JCN Corp.       $10     $13
 *                                      Macroware, Inc. $18     $47
 *                                      Pear, Inc.      $15     $45
 *
 * Your supervisor asks you to write an algorithm for computing the best way to purchase stocks,
 * given the initial money total, the number count of companies with stock available, an array start
 * containing the prices of each stock in 1991, and an array end containing the prices of each stock
 * in 2011. All prices are assumed to be positive integers
 *
 * -------------------------------------------------------------------------------------------------
 *
 * With all that sorted out, you submit the code to your supervisor and pat yourself on the back for
 * a job well done. Unfortunately, your supervisor comes back a few days later with a complaint
 * from the higher-ups. They’ve been playing with your program, and were very upset to discover
 * that when they ask what to do with $1,000,000,000 in the year 1991, it tells them to buy tens
 * of millions of shares in Dale, Inc. According to them, there weren’t that many shares of Dale
 * available to purchase. They want a new feature: the ability to pass in limits on the number of
 * stocks purchaseable.
 */
public class MaxStockProfit {

    //Initialize the stocks database
    static final int INFINITE = Integer.MAX_VALUE; //Not really infinite, but does the work.
    static int[] start = {12, 10, 18, 15};
    static int[] end =   {39, 13, 47, 45};
    static int[] limits = {4, 20, 30, 40};
    static int[][] profit;
    static int[] leftOverRecord;
    static boolean[][] purchase;
    static int count;

    public static void main (String [] args){
        //User input
        Scanner scanner = new Scanner(System.in);
        System.out.print("Type in the total cash: ");
        int total = scanner.nextInt();

        //Initialize arrays & variables
        count = start.length;
        profit = new int[total][count];
        purchase = new boolean[total][count];
        leftOverRecord = new int[total];

        maxStockProfit(total);
        System.out.println("No limits \n");
        printResults(total);

        //STOCKS WITH LIMITS EXAMPLE ----------------------------

        //Re-initialize arrays & variables
        count = start.length;
        profit = new int[total][count];
        purchase = new boolean[total][count];
        leftOverRecord = new int[total];

        maxStockProfitLimits(total);
        System.out.println("Limited amount of stocks \n");
        printResults(total);
    }

    /**
     * Method that does the recursion.
     * It returns the maximum possible profit using a certain cash
     * Doesn't takes limits into account.
     *
     * @param cash
     * @return maximum profit
     */
    private static int maxStockProfit(int cash){
        if(cash==0){
            return 0;
        }

        int i = cash-1;
        if(profit[i][count-1]!=0){
            return profit[i][count-1]; //memoization
        }

        for(int stock=0; stock<count; stock++){
            if(stock==0){
                profit[i][stock]=cash;
            } else {
                profit[i][stock]=profit[i][stock-1];
            }
            if(cash>=start[stock]){
                int leftOver = cash - start[stock];
                int profitLeftOver = maxStockProfit(leftOver); //recursive call
                int current = end[stock] + profitLeftOver;
                if(current>profit[i][stock]){
                    profit[i][stock]=current;
                    purchase[i][stock]=true;
                    leftOverRecord[i]=leftOver;
                }
            }
        }
        return profit[i][count-1];
    }

    /**
     * Method that prints the output of the algorithm
     * @param cash
     */
    private static void printResults(int cash){
        int i = cash-1;
        int [] results = new int[count];
        int leftOverCash=0;

        while(i>0){
            for(int stock=count-1; stock>=0; stock--){
                //Find the last stock that was added to purchase when having cash=i+1.
                //If it's the last stock that was added, it's because it gave more profit than all previous and posterior stocks.
                if(purchase[i][stock]){
                    results[stock]++;
                    leftOverCash =leftOverRecord[i];
                    break;
                }
            }
            //i becomes the leftOver cash we had when buying the stock.
            i=leftOverRecord[i]-1;
        }

        System.out.println("\n\nTotal profits: " +profit[cash-1][count-1]);
        for(int stock=0; stock<count; stock++){
            System.out.println("Stock " +(stock+1) +" - number of stocks bought: " +results[stock]);
        }
        System.out.println("Unused cash: " +leftOverCash);
    }



    /**
     * Method that does the recursion.
     * It returns the maximum possible profit using a certain cash.
     * Takes limits into account.
     * @param cash
     * @return maximum profit
     */
    private static int maxStockProfitLimits(int cash){
        //Base case: If don't have cash, then you can't make profit.
        if(cash<=0){
            return 0;
        }

        int i = cash-1;
        if(profit[i][count-1]!=0){
            return profit[i][count-1]; //memoization
        }

        for(int stock=0; stock<count; stock++){
            if(stock==0){
                profit[i][stock]=cash; //Untouched cash
            } else {
                profit[i][stock]=profit[i][stock-1]; //Profits from last stock
            }
            if(cash>=start[stock]) {
                if (limits[stock] > 0) {
                    limits[stock]--; //Take the stock so it can't be bought at the recursive call.
                    int leftOver = cash - start[stock];
                    int profitLeftOver = maxStockProfitLimits(leftOver); //recursive call
                    int current = end[stock] + profitLeftOver;
                    if (current > profit[i][stock]) {
                        profit[i][stock] = current;
                        purchase[i][stock] = true;
                        leftOverRecord[i] = leftOver;
                    } else {
                        limits[stock]++; //Return the stock if it wasn't bought.
                    }
                }
            }
        }
        return profit[i][count-1];
    }
}
