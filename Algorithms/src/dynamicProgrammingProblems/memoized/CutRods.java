package dynamicProgrammingProblems.memoized;

import java.util.Hashtable;
import java.util.Scanner;

/**
 * PROBLEM OBTAINED FROM:
 * Introduction to Algorithms, Cormen
 *
 * Serling Enterprises buys long steel rods and cuts them into shorter rods, which it then sells.
 * Each cut is free. The management of Serling Enterprises wants to know the best way to cut up
 * the rods.
 *
 * We assume that we know, for i= 1, 2,..., n the price pi in dollars that Serling number of inches.
 *
 * The rod-cutting problem is the following. Given a rod of length n inches and a
 * table of prices pi for i = 1, 2,..., n determine the maximum revenue rn obtainable by cutting
 * up the rod and selling the pieces. Note that if the price pn for a rod of length n is large enough,
 * an optimal solution may require no cutting at all.
 *
 * Example
 * length i 1   2   3   4   5    6    7    8    9    10
 * price pi 1   5   8   9   10   17   17   20   24   30
 */
public class CutRods {

    static int[] prices = {1, 5, 8, 9, 10, 17, 17, 20, 24, 30};
    static Hashtable<Integer, Integer> maximumRevenue;

    public static void main (String [] args){
        //User input
        Scanner scanner = new Scanner(System.in);
        System.out.print("Type in the length of the rod: ");
        int total = scanner.nextInt();

        maximumRevenue = new Hashtable<>();
        int max = cutRod(total);
        System.out.println(max);
    }

    /**
     * Method that returns the maximum revenue possible
     * @param distance
     * @return max revenue
     */
    public static int cutRod(int distance){
        //Base case
        if(distance==0){
            return 0;
        }

        if(maximumRevenue.containsKey(distance)){
            return maximumRevenue.get(distance);
        }

        for(int i=1; i<=distance; i++){
            int noCutPrice = 0;

            //If you can't sell a rod that is super long, then the noCutPrice remains 0.
            if(distance<=prices.length){
                noCutPrice = prices[distance-1];
            }

            int max = Math.max(noCutPrice, cutRod(distance-i) +prices[i-1] );
            if(!maximumRevenue.containsKey(distance)||maximumRevenue.get(distance)<max) {
                maximumRevenue.put(distance, max); //Memoization
            }
        }

        return  maximumRevenue.get(distance);
    }
}
