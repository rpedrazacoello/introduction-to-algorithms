package dynamicProgrammingProblems.memoized;

import dynamicProgrammingProblems.utility.HashtableKey;

import java.util.Date;
import java.util.Hashtable;


/**
 * PROBLEM OBTAINED FROM:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/lecture-21-dp-iii-parenthesization-edit-distance-knapsack/
 *
 * Given two strings x & y, what is the cheapest possible sequence of character edits (insert
 * c, delete c, replace c → c’) to transform x into y?
 *
 * cost of edit depends only on characters c, c0
 *
 */
public class EditDistance {

    static Hashtable<String, Integer> characterPosition;
    static int[] insertCost={3,5,6,1,8,4,6,3,0,7,2,7,4,8,6,3,1,4,7,3,9,5,3,8,6,2,0};
    static int[] deleteCost={5,8,4,2,9,6,4,7,8,9,5,2,6,4,3,2,1,6,4,9,2,5,3,7,5,4,7};
    static int[] replaceCost={6,4,2,7,8,5,3,2,6,9,5,4,2,6,4,7,4,1,9,7,8,5,3,6,2,5,2};
    static Hashtable<HashtableKey, Integer> optimalValue;
    static Hashtable <HashtableKey, HashtableKey> parentPointer;
    static Hashtable <HashtableKey, String> optimalOperation;
    static final String EMPTY_STRING = "";
    static String x = "hieroglyphology";
    static String y = "michaelangelo";

    public static void main (String [] args){
        optimalValue = new Hashtable<>();
        characterPosition = new Hashtable<>();
        parentPointer = new Hashtable<>();
        optimalOperation = new Hashtable<>();
        System.out.println(new Date());
        fillCharacterPositions();
        System.out.println("\n\t\tTurn string " +x.toUpperCase() +" into " +y.toUpperCase());
        System.out.println("\nEdit distance cost: " +editDistance(0,0));
        System.out.println("\nLeft to Right optimal operations:\n");
        printResults(new HashtableKey(0,0));
        System.out.println(new Date());
    }


    /**
     * Recurrence looks something like
     * DP(i,j)=min(
     *         (DP(i,j+1) +insertCost(i, j)),
     *         (DP(i+1, j) +deleteCost(i, j)),
     *         (DP(i+1, j+1) +replaceCost(i, j))
     *          )
     *
     * The base case is:
     * DP(n,m) = 0.
     * Where n = |x| and m = |y|
     *
     * @param i
     * @param j
     * @return min edit distance cost
     */
    public static int editDistance(int i, int j){
        HashtableKey rootKey = new HashtableKey(i, j);

        if(optimalValue.containsKey(rootKey)){
            return optimalValue.get(rootKey); //Memoization
        }

        //Base Case
        if(i==x.length() && j==y.length()){
            optimalValue.put(rootKey, 0); //Memoization

            //Parent Pointers
            parentPointer.put(rootKey,rootKey);
            optimalOperation.put(rootKey, EMPTY_STRING);
            return optimalValue.get(rootKey);
        }

        int costInsert = Integer.MAX_VALUE;
        int costDelete = Integer.MAX_VALUE;
        int costReplace = Integer.MAX_VALUE;
        int totalCostInsert = Integer.MAX_VALUE;
        int totalCostDelete = Integer.MAX_VALUE;
        int totalCostReplace = Integer.MAX_VALUE;

        if(j<y.length()) { //If j>=y.length, there are no more characters in y, so it's impossible to insert to x.
            costInsert = costOfInsert(i, j);
            totalCostInsert = costInsert + editDistance(i, j + 1);
        }

        if(i<x.length()) { //If i>=x.length, the are no more characters in x, so it's impossible to delete from x
            costDelete = costOfDelete(i, j);
            totalCostDelete = costDelete + editDistance(i + 1, j);
        }

        if(i<x.length() && j<y.length()) { //You need at least one character left in each String to make a replacement
            costReplace = costOfReplace(i, j);
            totalCostReplace = costReplace + editDistance(i + 1, j + 1);
        }

        if(costInsert<costDelete && costInsert<costReplace){
            optimalValue.put(rootKey, totalCostInsert); //Memoization

            //Parent Pointers
            parentPointer.put(rootKey, new HashtableKey(i, j+1));
            optimalOperation.put(rootKey, y.substring(j, j+1).toUpperCase() +" added. Cost:" +costInsert);
        } else if (costDelete<costReplace){
            optimalValue.put(rootKey, totalCostDelete); //Memoization

            //Parent Pointers
            parentPointer.put(rootKey, new HashtableKey(i+1, j));
            optimalOperation.put(rootKey, x.substring(i, i+1).toUpperCase() +" deleted . Cost:" +costDelete);
        } else {
            optimalValue.put(rootKey, totalCostReplace); //Memoization

            //Parent Pointers
            parentPointer.put(rootKey, new HashtableKey(i+1, j+1));
            optimalOperation.put(rootKey, x.substring(i, i+1).toUpperCase() +" replaced by " +y.substring(j, j+1).toUpperCase() +". Cost: "+costReplace);
        }

        return optimalValue.get(rootKey);
    }

    /**
     * Print the results
     * @param rootKey
     */
    private static void printResults(HashtableKey rootKey){
        HashtableKey parentKey = parentPointer.get(rootKey);
        if(!optimalOperation.get(rootKey).equals(EMPTY_STRING)) {
            System.out.println("\t\t"+optimalOperation.get(rootKey));
        }
        if(parentKey!=rootKey){
            printResults(parentKey);
        }
    }

    /**
     *
     * @param j
     * @return Cost of inserting character y[j] to x
     */
    private static int costOfInsert(int i, int j){
        String characterY = y.substring(j, j+1);
        int cPosition = characterPosition.get(characterY);
        return insertCost[cPosition];
    }

    /**
     *
     * @param i
     * @return Cost of deleting character x[i] from x
     */
    private static int costOfDelete(int i, int j){
        String characterX = x.substring(i, i+1);
        int cPosition = characterPosition.get(characterX);
        return deleteCost[cPosition];
    }

    /**
     * @param i
     * @return Cost of replacing character x[i] from x
     */
    private static int costOfReplace(int i, int j){
        String characterX = x.substring(i, i+1);
        String characterY = y.substring(j, j+1);
        if (characterX.equals(characterY)) {
            return 0;
        }
        int cPosition = characterPosition.get(characterX);
        return replaceCost[cPosition];
    }

    private static void fillCharacterPositions(){
        characterPosition.put("a",0);
        characterPosition.put("b",1);
        characterPosition.put("c",2);
        characterPosition.put("d",3);
        characterPosition.put("e",4);
        characterPosition.put("f",5);
        characterPosition.put("g",6);
        characterPosition.put("h",7);
        characterPosition.put("i",8);
        characterPosition.put("j",9);
        characterPosition.put("k",10);
        characterPosition.put("l",11);
        characterPosition.put("m",12);
        characterPosition.put("n",13);
        characterPosition.put("o",14);
        characterPosition.put("p",15);
        characterPosition.put("q",16);
        characterPosition.put("r",17);
        characterPosition.put("s",18);
        characterPosition.put("t",19);
        characterPosition.put("u",20);
        characterPosition.put("v",21);
        characterPosition.put("w",22);
        characterPosition.put("x",23);
        characterPosition.put("y",24);
        characterPosition.put("z",25);
    }
}
