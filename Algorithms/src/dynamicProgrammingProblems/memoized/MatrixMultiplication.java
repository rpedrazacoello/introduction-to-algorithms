package dynamicProgrammingProblems.memoized;

import java.util.Hashtable;

/**
 * PROBLEM OBTAINED FROM:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/lecture-21-dp-iii-parenthesization-edit-distance-knapsack/
 *
 * Optimal evaluation of associative expression A[0] · A[1] · · · A[n − 1]
 */
public class MatrixMultiplication {

    //DIMENSION OF THE MATRICES
    static Integer [][] matrices = {{50,5},{5,100},{100,10},{10,100}, {100,5}, {5,50}};
    static Integer [] emptyArray = {0,0};
    static Hashtable<String, Integer> optimal;
    static Hashtable<String, Integer[]> optimalArrays;
    static Hashtable<String, String> parentPointer;
    static final int NO_MATRIX = Integer.MAX_VALUE;
    static final int NO_OPERATION = 0;

    public static void main (String [] args){
        optimal = new Hashtable<>();
        optimalArrays = new Hashtable<>();
        parentPointer = new Hashtable<>();
        System.out.println("Minimum number of operations: " +minimumOperation(0, matrices.length));
        System.out.println("\nMatrix multiplication form: ");
        System.out.println(printResult(0, matrices.length));
    }

    private static int minimumOperation(int start, int finish){
        String rootKey = createKey(start, finish);

        if(optimal.containsKey(rootKey)){
            return optimal.get(rootKey); //Memoization
        }

        if(start==finish-1){ //If the start,finish positions surround a single matrix
            optimal.put(rootKey, NO_OPERATION);
            optimalArrays.put(rootKey, matrices[start]);
            return optimal.get(rootKey);
        }

        if(start == finish){ //If the start,finish position doesn't surround matrices
            optimal.put(rootKey, NO_MATRIX);
            optimalArrays.put(rootKey, emptyArray);
            return optimal.get(rootKey);
        }

        for(int i=start; i<finish; i++){
            for(int j=i+1; j<=finish; j++){

                String leftKey = createKey(start, j); //Surround matrices from start to j-1
                int leftOperationCost = minimumOperation(start,j);

                String rightKey = createKey(j, finish); //Surround matrices from j to finish-1
                int rightOperationCost = minimumOperation(j, finish);

                int totalCost = NO_MATRIX;
                if(leftOperationCost!=NO_MATRIX && rightOperationCost!=NO_MATRIX) {
                    totalCost = leftOperationCost + rightOperationCost + costOfMultypling(leftKey, rightKey);
                }

                if((!optimal.containsKey(rootKey) || optimal.get(rootKey)>totalCost)
                        && totalCost< NO_MATRIX){
                    optimal.put(rootKey, totalCost);//Memoization

                    //Parent Pointers
                    optimalArrays.put(rootKey, createNewMatrix(leftKey, rightKey));
                    parentPointer.put(rootKey, leftKey+":"+rightKey);
                }
            }
        }
        return  optimal.get(rootKey);
    }

    /**
     * Methos that creates keys for the hashtables
     * @param i
     * @param j
     * @return
     */
    private static String createKey(int i, int j){
        return i+","+j;
    }

    /**
     * Calculate the cost of multiplying the array identified by the left key * array identified by the right key
     * @param leftKey
     * @param rightKey
     * @return
     */
    private static int costOfMultypling(String leftKey, String rightKey){
        Integer [] matrix1 = optimalArrays.get(leftKey);
        Integer [] matrix2 = optimalArrays.get(rightKey);

        if(matrix1.equals(emptyArray)){
            return Integer.MAX_VALUE;
        } else if (matrix2.equals(emptyArray)){
            return Integer.MAX_VALUE;
        }

        return matrix1[0] * matrix1[1] * matrix2[1];
    }

    /**
     * Method that returns the matrix size of the resulted multiplication of
     * matrices identified by leftKey and rightKey
     * @param leftKey
     * @param rightKey
     * @return
     */
    private static Integer[] createNewMatrix(String leftKey, String rightKey){
        Integer [] matrix1 = optimalArrays.get(leftKey);
        Integer [] matrix2 = optimalArrays.get(rightKey);

        if(matrix1.equals(emptyArray)){
            return matrix2;
        } else if (matrix2.equals(emptyArray)){
            return matrix1;
        }

        Integer [] temp = {matrix1[0], matrix2[1]};
        return temp;
    }

    /**
     * Returns optimal parenthesization of the matrices
     * @param i
     * @param j
     * @return
     */
    private static String printResult (int i, int j){
        String rootKey = createKey(i, j);
        if(parentPointer.containsKey(rootKey)) {
            String[] temp = parentPointer.get(rootKey).split(":");
            String[] leftArray = temp[0].split(",");
            String[] rightArray = temp[1].split(",");
            String left = "";
            String right = "";
            if (Integer.valueOf(leftArray[0] + 1) == Integer.valueOf(leftArray[1])) {
                left ="(" + matrices[Integer.valueOf(leftArray[0])][0] + ", " + matrices[Integer.valueOf(leftArray[1])][1] + ")";
            } else {
                left = printResult(Integer.valueOf(leftArray[0]), Integer.valueOf(leftArray[1]));
            }

            if (Integer.valueOf(rightArray[0] + 1) == Integer.valueOf(rightArray[1])) {
                right = "(" + matrices[Integer.valueOf(rightArray[0])] + ", " + matrices[Integer.valueOf(rightArray[1])] + ")";
            } else {
               right = printResult(Integer.valueOf(rightArray[0]), Integer.valueOf(rightArray[1]));
            }
            return "(" +left +right +")";
        } else {
            return "("+matrices[i][0]+","+matrices[i][1]+")";
        }
    }
}
