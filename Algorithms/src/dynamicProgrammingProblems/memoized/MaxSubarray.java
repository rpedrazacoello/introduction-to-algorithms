package dynamicProgrammingProblems.memoized;

import dynamicProgrammingProblems.utility.HashtableKey;

import java.util.Hashtable;

/**
 * ORIGINAL INSTRUCTIONS:
 * Sea A[1,...,n] un arreglo con enteros tanto positivos como negativos en sus entradas.
 * La suma de un subarreglo A[i,....,j] es la suma de sus entradas: sum(A[k]) -> range(i,j).
 * El problema del subarreglo de suma máxima consiste en recibir A como entrada y devolver el subarreglo de A cuya suma
 * sea la máxima posible de entre todos los subarreglos de A.
 *
 *
 * TRANSLATION:
 * Being A[1,...,n] an array of integers both positive and negative.
 * The sum of a subarray A[i,...,j] is the sum of it's entries: sum(A[k]) -> for every i<=k<=j
 * The problem of the subarray of maximum sum consists on receiving A as an entry, and returning the subarray of A
 * which sum is the maximum possible between every subarray of A.
 */
public class MaxSubarray {

    //static int[] A = {38, 93, -95, -64, 59, -56, 24, -23, -39, 41, 0, 90, 52, -57, 100, -22, 25, 75, -73, -6, 1, 69, 64, -70, 37, -59, -46, -62, -40, -77, -9, 42, 12, -43, -32, -18, -47, -27, 68, 63, -33, 79, -12, 81, 94, -61, -14, 28, -84, -38, 66, -94, 51, 2, 96, -48, -71, -99, 23, 39, -87, 62, -86, -36, -89, -96, -82, -79, 6, -90, -21, -19, -69, -3, -1, -50, -45, 40, -29, -76, 88, -13, -60, -20, -30, -75, -67, 50, -98, 32, 9, -11, 35, -31, 17, -80, 8, -44, 98, 87, -85, -97, 76, 7, 92, -4, 4, 47, 60, 99, 80, 19, -81, -24, -25, -63, 29, -83, -10, 95, -15, 61, 71, 14, -66, -2, -17, 82, 36, -88, 54, -8, 5, 43, -34, 55, -93, -55, 73, 85, 91, -28, -65, -7, 97, -100, 26, 33, -91, -5, 30, 22, 31, 46, 49, 15, -37, 83, 77, 13, 78, 57, -92, 70, 56, 53, 3, -41, 89, 44, -53, 20, -51, -78, 21, 72, 10, 84, -54, -72, 27, 74, 16, -26, 65, -35, -52, -74, 86, 18, 48, -16, -42, 58, 67, 45, 34, 11, -68, -49, -58};
    static int[] A = {-38, -93, -95, -64, 59, -56, 24, -23, -39, -41, 0, 90, 52, 57, 100, 0, -22};
    //static int[] A = {-5, 10, 3, 6, -5};
    //static int[] A = {-5};
    static Hashtable<Integer, Integer> optimalValue;
    static Hashtable<Integer, Integer> parentPointers;
    static int maxSubarrayValue = Integer.MIN_VALUE;
    static int [] maxSubarray = {0,0};
    static final int NO_PARENT = -1;

    public static void main(String[] args) {
        optimalValue = new Hashtable<>();
        parentPointers = new Hashtable<>();
        maxSubarray(A.length-1);
        printResults();
    }

    /**
     * Returns the max(A[i] + .... + A[finish]) where i goes from finish-1 to 0.
     * @param finish
     * @return
     */
    private static int maxSubarray(int finish){
        if(optimalValue.containsKey(finish)){
            return optimalValue.get(finish); //Memoization
        }

        if(finish==0){ //Base Case
            optimalValue.put(finish, A[finish]);
            parentPointers.put(finish, finish);
            return optimalValue.get(finish);
        }

        int sum1 = maxSubarray(finish-1) + A[finish];
        int sum2 = A[finish];

        if(sum1 > sum2){
            optimalValue.put(finish, sum1); //Memoization

            //Parent Pointers
            parentPointers.put(finish, parentPointers.get(finish-1));
        } else {
            optimalValue.put(finish, sum2); //Memoization

            //Parent Pointers
            parentPointers.put(finish, finish);
        }

        if (maxSubarrayValue < sum1 || maxSubarrayValue < sum2) {
            maxSubarrayValue = Math.max(sum1, sum2);

            //Parent Pointers
            if(sum2>sum1){
                maxSubarray[0]=finish;
            } else {
                int f1 = finish-1;
                int f2 = parentPointers.get(f1);
                while(f1!=f2){
                    f1 = f2;
                    f2=parentPointers.get(f1);
                }
                maxSubarray[0]=f1;
            }
            maxSubarray[1]=finish;
        }

        return  optimalValue.get(finish);
    }

    private static void printResults (){
        System.out.println("Max Subarray value: " +maxSubarrayValue);
        System.out.println("["+maxSubarray[0] +":" +(maxSubarray[1]+1)+"]");
        System.out.print("{" +A[maxSubarray[0]]);
        for(int i=(maxSubarray[0]+1);i<=maxSubarray[1];i++){
            System.out.print(", " +A[i]);
        }
        System.out.print("}");
    }
}
