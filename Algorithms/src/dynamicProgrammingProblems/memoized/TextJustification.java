package dynamicProgrammingProblems.memoized;

import java.util.Hashtable;

/**
 * PROBLEM OBTAINED FROM:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/lecture-20-dynamic-programming-ii-text-justification-blackjack/
 *
 * LECTURE NOTES:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/MIT6_006F11_lec20.pdf
 */
public class TextJustification {


    static String [] words = {"This", "is", "an", "example", "of", "how", "the", "text", "justification", "algorithm",
    "works", "with", "dynamic", "programming.", "The", "idea", "is", "to", "create", "a", "justified", "text", "with",
    "the", "smaller", "gaps", "possible", "between", "words", "across", "the", "document"};

    //Number of possible characters in a line
    static int pageWidht=50;
    static Hashtable<Integer, Integer> optimalSolution;

    /**
     * Parent Pointer
     * Parent[0]=index of word that started 2nd line
     * Parent[Parent[0]]=index of word that started the 3rd line*/
    static Hashtable<Integer, Integer> parent;

    /**spaces[0] = number of extra spaces in line that start with word[0]*/
    static Hashtable<Integer, Integer> spaces;

    public static void main (String [] args){

        parent = new Hashtable<>();
        spaces = new Hashtable<>();
        optimalSolution = new Hashtable<>();

        int min = textJustification(0);
        System.out.println("\nBadness: " +min);
        printText();
    }

    /**
     * Method that receives the index of a word, and minimizes the badness
     * of having that word as a new line.
     * @param wordIndex
     * @return badness of having words[wordIndex] as a new line
     */
    private static int textJustification(int wordIndex){
        if(optimalSolution.containsKey(wordIndex)){
            return optimalSolution.get(wordIndex); //Memoization
        }

        //Base Case
        if(wordIndex==words.length){
            //Indices goes from 0 to n-1, if the index==n, then there are no more words left
            optimalSolution.put(wordIndex, 0);
        }

        else {
            int lenght=words[wordIndex].length();
            int lengthNewWordInLine = 0;
            int nextNewLine = wordIndex+1;

            //i is the index of the new line
            for(int i=nextNewLine; i<=words.length; i++) {
                int badness;

                //If i==nextNewLine then there will not be a new word added to the line.
                if (i == nextNewLine) {
                    badness = badness(lenght);
                } else {
                    //+1 because of the space between the two words
                    lenght = lenght + lengthNewWordInLine + 1;
                    badness = badness(lenght);
                }

                //There are too many words in the line.
                if(badness==Integer.MAX_VALUE){
                    break;
                }

                //Badness of this line + badness of posterior lines
                badness = badness + textJustification(i); //Recursion

                if (!optimalSolution.containsKey(wordIndex) || optimalSolution.get(wordIndex) > badness) {
                    optimalSolution.put(wordIndex, badness); //Memoization

                    //Parent Pointers
                    parent.put(wordIndex, i);
                    spaces.put(wordIndex, pageWidht-lenght);
                }

                //If i<words.length then there are more words to process
                if(i<words.length) {
                    lengthNewWordInLine = words[i].length();
                }
            }
        }
        return optimalSolution.get(wordIndex);
    }

    /**
     * Calculates the badness of a length of characters in a line
     * @param lenght
     * @return badness
     */
    private static int badness(int lenght){
        if(pageWidht>=lenght) {
            return (int)Math.pow((pageWidht - lenght), 3); //Punishes big gaps in a line
        } else {
            return Integer.MAX_VALUE;
        }
    }

    /**
     * Prints justified text
     */
    private static void printText(){
        int wordLine = 0;
        while(wordLine<words.length){
            System.out.print("\n");
            int start = wordLine;
            int finish = parent.get(wordLine)-1;
            int numberSpace = spaces.get(start);

            //Add space between words
            //Not adding spaces to the last word of the line
            for(int i=start; i<finish; i++){
                words[i]=words[i]+" ";
            }

            //Adding the extra spaces
            //Not adding spaces to the last word of the line
            int i=start;
            while(numberSpace>0){
                if(i>=finish-1 && numberSpace !=0){
                    i=start;
                }

                if(numberSpace>0){
                    numberSpace--;
                    words[i]=words[i]+" ";
                }
                i++;
            }

            //Print text
            for(int j=start; j<=finish; j++){
                System.out.print(words[j]);
            }

            wordLine=parent.get(wordLine);
        }
        System.out.println("\n");
    }
}
