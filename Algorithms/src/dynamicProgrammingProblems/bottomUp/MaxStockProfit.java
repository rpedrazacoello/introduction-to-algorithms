package dynamicProgrammingProblems.bottomUp;

import java.util.Scanner;

/**
 * PROBLEM OBTAINED FROM:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/assignments/MIT6_006F11_ps7.pdf
 *
 * You have been given an internship at the extremely profitable and secretive bank HG Fargo. Your
 * immediate supervisor tells you that higher-ups in the bank are very interested in learning from the
 * past. In particular, they want to know how much money they could have made if they had invested
 * optimally.
 *
 * Your supervisor gives you the following data on the prices1 of select stocks in 1991 and in 2011:
 * Company Price in 1991 Price in 2011
 *                                      Dale, Inc.      $12 $39
 *                                      JCN Corp.       $10 $13
 *                                      Macroware, Inc. $18 $47
 *                                      Pear, Inc.      $15 $45
 *
 * Your supervisor asks you to write an algorithm for computing the best way to purchase stocks,
 * given the initial money total, the number count of companies with stock available, an array start
 * containing the prices of each stock in 1991, and an array end containing the prices of each stock
 * in 2011. All prices are assumed to be positive integers
 */
public class MaxStockProfit {

    //Initialize the stocks database
    static int[] start = {12, 10, 18, 15};
    static int[] end =   {39, 13, 47, 45};
    static int count;
    static int[][] profit;
    static byte[][] purchase;
    static int[] leftOverRecord;

    public static void main (String [] args){
        Scanner scanner = new Scanner(System.in);

        System.out.print("Type in the total cash: ");
        int total = scanner.nextInt();

        count = start.length;
        profit = new int[total][count];
        purchase = new byte[total][count];
        leftOverRecord = new int[total];

        maxStockProfit(total);
        printResult(total);
    }

    private static void maxStockProfit(int total){
        for(int i=0; i<total; i++){
            //Amount of money goes bottom up
            int cash = i+1;

            //In the worst case scenario, you profit = cash
            profit[i][0]=cash;

            //The leftover you get when i=0
            if(i==0){
                leftOverRecord[i]=0;
            }

            //for every stock
            for(int stock=0; stock<count; stock++){
                // The worst case scenario is that the stock profit = cash
                if(stock==0){
                    profit[i][stock]=cash;
                } else {
                    //With this we can always keep the largest profit at profit[i][count]
                    profit[i][stock]=profit[i][stock-1];
                }
                //If you can buy a stock with the cash
                if(start[stock]<=cash){
                    //Calculate leftOver after buying the cash
                    int leftOver = cash - start[stock];
                    //Calculate the profit of using the leftOver
                    int leftOverProfit = leftOver > 0 ? profit[leftOver-1][stock]: 0;
                    int current = end[stock] + leftOverProfit;
                    //If the current profit is higher than the old profit
                    if(profit[i][stock]<current){
                        purchase[i][stock]=1;
                        profit[i][stock]=current;
                        leftOverRecord[i]=leftOver;
                    }
                }
            }
        }
    }

    private static void printResult(int total){
        int [] results = new int[count];
        int i=total-1;
        int leftOverCash=0;

        while(i>0){
            for(int stock=count-1; stock>=0; stock--){
                //Find the last stock that was added to purchase when having cash=i+1.
                //If it's the last stock that was added, it's because it gave more profit than all previous and posterior stocks.
                if(purchase[i][stock]==1){
                    results[stock]++;
                    leftOverCash =leftOverRecord[i];
                    break;
                }
            }
            //i becomes the leftOver cash we had when buying the stock.
            i=leftOverRecord[i]-1;
        }

        System.out.println("\n\nTotal profits: " +profit[total-1][count-1]);
        for(int stock=0; stock<count; stock++){
            System.out.println("Stock " +(stock+1) +" - number of stocks bought: " +results[stock]);
        }
        System.out.println("Left over cash: " +leftOverCash);
    }
}
