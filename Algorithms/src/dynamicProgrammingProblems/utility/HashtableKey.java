package dynamicProgrammingProblems.utility;

import java.util.Objects;

public class HashtableKey {

    private int i;
    private int j;

    public HashtableKey(int i, int j) {
        this.i = i;
        this.j = j;
    }

    public int getI() {
        return i;
    }

    public int getJ() {
        return j;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HashtableKey)) return false;
        HashtableKey that = (HashtableKey) o;
        return getI() == that.getI() &&
                getJ() == that.getJ();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getI(), getJ());
    }
}
