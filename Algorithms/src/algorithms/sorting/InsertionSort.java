package algorithms.sorting;

/**
 * EXPLANATION VIDEO:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/lecture-3-insertion-sort-merge-sort/
 *
 * LECTURE NOTES:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/MIT6_006F11_lec03.pdf
 */
public class InsertionSort {

    int size;
    int [] a;
    int count = -1;

    /**
     * Constructor Method
     * @param size - Size of the array that will be
     */
    public InsertionSort(int size) {
        this.a = new int[size];
        this.size=a.length;
    }

    /**
     * Insert an int value into the sorted array
     * @param value - value to be added
     */
    public void insert (int value){
        if(count == -1) {
            count = 0;
            a[0] = value;
        }
        else if(count<=size-1){
          count++;
          if(count>=a.length){
              System.out.println("The array is full");
              return;
          }
          a[count]=value;
          for (int key=1; key<=count; key++){
              for(int i=key; i>0; i--){
                  if(a[i]<a[i-1]){
                      int temp = a[i];
                      a[i]=a[i-1];
                      a[i-1]=temp;
                  }
              }
          }
        }
    }

    public void printArray(){
        System.out.println("Array Ordenado: ");
        for (int i=0; i<=count; i++){
            System.out.print(a[i] + ", ");
        }
    }
}
