package algorithms.sorting;

/**
 * EXPLANATION VIDEO:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/lecture-3-insertion-sort-merge-sort/
 *
 * LECTURE NOTES:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/MIT6_006F11_lec03.pdf
 */
public class MergeSort {

    /**
     * Method that receives an unsorted array and returns a sorted array.
     * @param a
     * @return sorted Array
     */
    public int[] sort(int [] a){
        return sort(a, 0, a.length-1);
    }

    /**
     * Recursive method that returns a sorted array
     * @param a
     * @param start
     * @param end
     * @return sorted array
     */
    private int[] sort(int [] a, int start, int end){
        if(start==end){
            int [] result = {a[start]};
            return result;
        }

        int leftStart = start, leftEnd = (start+end)/2;
        int rightStart = leftEnd+1, rightEnd = end;
        int [] leftResult = sort(a, leftStart, leftEnd);
        int [] rightResult = sort(a, rightStart, rightEnd);
        int [] result = new int [leftResult.length + rightResult.length];
        int leftCount=0, rightCount=0;


        for(int i=0; i<result.length; i++){
            if (leftCount == leftResult.length){
                result[i] = rightResult[rightCount];
                rightCount++;
                continue;
            }

            if(rightCount == rightResult.length){
                result[i]=leftResult[leftCount];
                leftCount++;
                continue;
            }

            if(leftResult[leftCount]<= rightResult[rightCount]){
                result[i]=leftResult[leftCount];
                leftCount++;
            } else {
                result[i]=rightResult[rightCount];
                rightCount++;
            }
        }

        return result;
    }

}
