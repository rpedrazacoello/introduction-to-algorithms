package algorithms.peak_finder;

/**
 * EXPLANATION VIDEO:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/lecture-1-algorithmic-thinking-peak-finding/
 *
 * LECTURE NOTES:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/MIT6_006F11_lec01.pdf
 */
public class OneDimensionPeakFinder {

    /**
     * Method that finds a peak in an array
     * @param a
     * @return Array position of the peak
     */
    public int findPeak(int[] a) {
        return findPeak(a, 0, a.length);
    }

    /**
     * Recursive method that finds a peak in an array
     * @param a
     * @param start
     * @param end
     * @return Array position of the peak
     */
    private int findPeak(int[] a, int start, int end) {
        int look = (start + end) / 2;
        if (look != 0)
            if (a[look] < a[look - 1])
                return findPeak(a, start, look - 1);

        if (look != a.length - 1)
            if (a[look] < a[look + 1])
                return findPeak(a, look + 1, end);

        return look;
    }
}
