package algorithms.peak_finder;

/**
 * EXPLANATION VIDEO:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/lecture-1-algorithmic-thinking-peak-finding/
 *
 * LECTURE NOTES:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/MIT6_006F11_lec01.pdf
 */
public class TwoDimensionPeakFinder {

    OneDimensionPeakFinder oneDimensionPeakFinder;

    public TwoDimensionPeakFinder() {
        oneDimensionPeakFinder = new OneDimensionPeakFinder();
    }

    /**
     * Method that finds a peak in a n*m array
     * @param a
     * @return Array with the coordinates of the peak.
     */
    public int[] peakFinder (int [][] a){
        int [] cordenadas = peakFinder(a, 0, a.length);
        return cordenadas;
    }

    /**
     * Recursive method to find a peak in a n*m array
     * @param a
     * @param start
     * @param end
     * @return
     */
    public int[] peakFinder (int [][] a, int start, int end){
        int look = (start + end)/2;
        int peak = oneDimensionPeakFinder.findPeak(a[look]);
        if(a[look][peak]<a[look][peak-1]){
            return peakFinder(a, start, peak-1);
        } else if(a[look][peak]<a[look][peak+1]) {
            return peakFinder(a, peak+1, end);
        } else {
            int [] result = {look, peak};
            return result;
        }
    }
}
