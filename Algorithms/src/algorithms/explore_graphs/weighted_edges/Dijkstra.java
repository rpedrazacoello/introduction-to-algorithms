package algorithms.explore_graphs.weighted_edges;

import algorithms.explore_graphs.AbstractGraphExplorer;
import data_structures.Heap.Heapify.HeapNode;
import data_structures.Heap.Heapify.MinHeapify;
import data_structures.graphs.Edge;
import data_structures.graphs.Node;
import data_structures.graphs.WeightGraph.WeightEdge;
import data_structures.graphs.WeightGraph.WeightNode;

import java.util.Hashtable;

public class Dijkstra extends AbstractGraphExplorer {

    /**
     * Root of Dijkstra
     */
    private String rootIdentifier;


    /**
     * Constructor for abstract class
     *
     * @param isDirectedGraph
     */
    public Dijkstra(boolean isDirectedGraph) {
        super(isDirectedGraph);
    }

    @Override
    public void run() {
        //Frontier where the visited nodes will be added
        MinHeapify frontier = new MinHeapify();
        //Helps keep control of the HeapNode's references
        Hashtable<String, HeapNode> frontierHelper = new Hashtable<>();

        //Setting the root's values
        WeightNode root = (WeightNode) nodes.get(rootIdentifier);
        root.ifNewDistanceLess_setNewDistance(0);
        root.setParentIdentifier(NO_PARENT);
        HeapNode heapNode = new HeapNode(root.getDistance(), root);
        frontier.addToHeap(heapNode);

        while (frontier.getRoot()!=null){
            //Obtain the minimum value of the Heap, this is a Shortest Path
            WeightNode current = (WeightNode) frontier.extractRootFromHeap().getValue();
            parent.put(current.getIdentifier(), current.getParentIdentifier());

            //Update the distance of the edges adjacent to current
            for (Edge e: current.getEdges()) {
                WeightEdge edge = (WeightEdge) e;
                WeightNode node = (WeightNode) e.getNodeConnectedTo(current);

                //If the shortest path for node hasn't been found
                if(!parent.containsKey(node.getIdentifier())){
                    //If the new distance is less than the old distance
                    if(node.ifNewDistanceLess_setNewDistance(current.getDistance() + edge.getWeight())){
                        node.setParentIdentifier(current.getIdentifier());
                        HeapNode hn = frontierHelper.get(node.getIdentifier());
                        //If node hasn't been added to the Min-heap, then this is the first visiting this node
                        if(hn==null){
                            hn = new HeapNode(node.getDistance(), node);
                            frontier.addToHeap(hn);
                            frontierHelper.put(node.getIdentifier(), hn);
                            //If the node has been visited, but the new distance is shorter
                        } else {
                            hn.setKey(node.getDistance());
                            frontier.updateFromHeap(hn);
                        }
                        frontierHelper.put(node.getIdentifier(), hn);
                    }
                }
            }
        }
    }

    /**
     * Set the starting point of the Dijkstra algorithm.
     * Every time you reset your rootIdentifier, you have to re-run Dijkstra
     * @param rootIdentifier - Root from where to start Dijkstra
     */
    public void setRootIdentifier(Node rootIdentifier) {
        this.rootIdentifier = rootIdentifier.getIdentifier();
    }


    /**
     * Prints the shortest path from root to node v
     * @param v
     */
    public void printRouteFromRootToNodeV(WeightNode v){
        super.printRouteFromRootToNodeV(v);
        System.out.println("Distance: " +v.getDistance());
    }

    /**
     * Adds an edge to the graph
     * @param edge
     */
    public void addEdge(WeightEdge edge){
        super.addEdge(edge);
    }

    /**
     * Adds a node to the graph
     * @param node
     */
    public void addNode(WeightNode node){
        super.addNode(node);
    }
}
