package algorithms.explore_graphs.weighted_edges;

import algorithms.explore_graphs.AbstractGraphExplorer;
import data_structures.graphs.WeightGraph.WeightEdge;
import data_structures.graphs.WeightGraph.WeightNode;

import java.util.ArrayList;

/**
 * LECTURE VIDEO:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/lecture-17-bellman-ford/
 *
 * LECTURE NOTES:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/MIT6_006F11_lec17.pdf
 *
 * The Bellman-Ford Algorithm for undirected graph is not implemented. This because any negative weight undirected edge
 * becomes a negative cycle.
 */
public class BellmanFord extends AbstractGraphExplorer {

    /**
     * Root of BellmanFord
     */
    private String rootIdentifier;
    private ArrayList<WeightEdge> edges;

    /**
     * Constructor Method
     */
    public BellmanFord() {
        super(true);
        edges = new ArrayList<>();
    }

    @Override
    public void run() {
        //Setting the root's values
        WeightNode root = (WeightNode) super.nodes.get(rootIdentifier);
        root.ifNewDistanceLess_setNewDistance(0);
        root.setParentIdentifier(NO_PARENT);
        parent.put(rootIdentifier, NO_PARENT);

        for(int i=1; i<=super.nodes.size()-1; i++){
            for (WeightEdge edge: edges) {
                if(this.isDirectedGraph){
                    bellmanFordDirected(edge);
                }
            }
        }

        //I run it 2 times so I can identify all the nodes that belong or are affected by a negative cycle
        //If you only run this one time you can identify that there's a negative cycle, but you can't identify
        //all the nodes affected by it.
        for(int i=0; i<=1; i++) {
            for (WeightEdge edge : edges) {
                if (this.isDirectedGraph) {
                    bellmanFordDirectedIdentifyNegativeCycles(edge);
                }
            }
        }
    }

    /**
     * Relaxation from the nodes connected by the edge.
     * @param edge
     */
    private void bellmanFordDirected(WeightEdge edge){
        WeightNode nodeFrom = (WeightNode) edge.getNode1();
        WeightNode nodeTo = (WeightNode) edge.getNode2();
        if (!nodeFrom.isInfiniteDistance()){
            if (nodeTo.ifNewDistanceLess_setNewDistance(nodeFrom.getDistance() + edge.getWeight())) {
                nodeTo.setParentIdentifier(nodeFrom.getIdentifier());
            }
        }
    }

    /**
     * Relaxation from the nodes connected by the edge.
     * @param edge
     */
    private void bellmanFordDirectedIdentifyNegativeCycles(WeightEdge edge){
        WeightNode nodeFrom = (WeightNode) edge.getNode1();
        WeightNode nodeTo = (WeightNode) edge.getNode2();

        if (!nodeFrom.isInfiniteDistance()){
            parent.put(nodeTo.getIdentifier(), nodeTo.getParentIdentifier());

            if (nodeTo.ifNewDistanceLess_setNewDistance(nodeFrom.getDistance() + edge.getWeight())) {
                nodeTo.setParentIdentifier(nodeFrom.getIdentifier());
                parent.remove(nodeTo);
                nodeTo.setUndetermined(true);
            }
        }
    }

    /**
     * Adds an edge to the graph
     * @param edge
     */
    public void addEdge(WeightEdge edge){
        super.addEdge(edge);
        edges.add(edge);
    }

    /**
     * Adds a node to the graph
     * @param node
     */
    public void addNode(WeightNode node){
        super.addNode(node);
    }

    /**
     * Set the starting point of the BellmandFord algorithm.
     * Every time you reset your rootIdentifier, you have to re-run BellmanFord
     * @param rootIdentifier - Root from where to start BellmanFord
     */
    public void setRootIdentifier(WeightNode rootIdentifier) {
        this.rootIdentifier = rootIdentifier.getIdentifier();
    }

    /**
     * Prints the shortest path from root to node v
     * @param v
     */
    public void printRouteFromRootToNodeV(WeightNode v){
        if(!v.isUndetermined()) {
            super.printRouteFromRootToNodeV(v);
            System.out.println("Distance: " + v.getDistance());
        } else {
            System.out.println("\nRoute from " + rootIdentifier + " to " + v.getIdentifier() + " is undetermined. " +
                    "\nThe node " + v.getIdentifier() + " is contained or affected by a negative cycle");
        }
    }
}
