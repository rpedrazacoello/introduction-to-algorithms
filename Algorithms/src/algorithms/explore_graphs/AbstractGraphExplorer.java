package algorithms.explore_graphs;

import data_structures.graphs.Edge;
import data_structures.graphs.Node;

import java.util.ArrayList;
import java.util.Hashtable;

public abstract class AbstractGraphExplorer {

    protected Hashtable<String, String> parent;
    protected Hashtable<String, Node> nodes;
    protected boolean isDirectedGraph;
    protected static final String NO_PARENT="NA";

    /**
     * Constructor for abstract class
     * @param isDirectedGraph
     */
    protected AbstractGraphExplorer(boolean isDirectedGraph){
        this.isDirectedGraph = isDirectedGraph;
        nodes = new Hashtable<>();
        parent = new Hashtable<>();
    }

    /**
     * Add a Node to the Graph.
     * After adding a node you need to re-run to update the Abstract Breadth Search.
     * This method is protected so that every Algorithm can filter
     * their Nodes. For example to only accept WeightNodes
     * @param node
     */
    protected void addNode(Node node){
        nodes.put(node.getIdentifier(), node);
    }

    /**
     * This methods add's edges to the Graphs.
     * This method is protected so that every Algorithm can filter
     * their Edges. For example to only accept WeightEdges.
     * @param edge
     */
    protected void addEdge(Edge edge){
        edge.getNode1().addEdge(edge);
        if(!isDirectedGraph) {
            edge.getNode2().addEdge(edge);
        }
    }

    /**
     * Method that runs the algorithms
     */
    public abstract void run();

    /**
     * Prints the route from the root to a certain node v
     * @param v - Node where trying to get
     */
    protected void printRouteFromRootToNodeV(Node v){
        if(!parent.containsKey(v.getIdentifier())){
            System.out.println("There is no route");
        } else {
            ArrayList<String> route = new ArrayList<>();
            String current = v.getIdentifier();
            while(!parent.get(current).equals(NO_PARENT)){
                route.add(current);
                if(!parent.get(current).equals(NO_PARENT)) {
                    current = parent.get(current);
                }
            }
            route.add(current);
            System.out.println("\nRoute from " +current +" to " +v.getIdentifier() +":");
            for(int i=route.size()-1; i>=0; i--){
                if(i==0){
                    System.out.print(route.get(i));
                } else {
                    System.out.print(route.get(i) + ", ");
                }
            }
            System.out.println("\n");
        }
    }
}
