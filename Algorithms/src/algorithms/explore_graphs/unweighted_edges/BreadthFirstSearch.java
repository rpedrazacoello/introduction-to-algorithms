package algorithms.explore_graphs.unweighted_edges;

import algorithms.explore_graphs.AbstractGraphExplorer;
import data_structures.graphs.Edge;
import data_structures.graphs.Node;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;

/**
 * LECTURE VIDEO:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/lecture-13-breadth-first-search-bfs/
 *
 * LECTURE NOTES:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/MIT6_006F11_lec13.pdf
 *
 */
public class BreadthFirstSearch extends AbstractGraphExplorer {

    /**
     * Root of BFS
     */
    private String rootIdentifier;

    /**
     * Constructor of BFS
     * @param isDirectedGraph
     */
    public BreadthFirstSearch(boolean isDirectedGraph) {
        super(isDirectedGraph);
    }

    /**
     * Method that runs the algorithms BFS
     * Every time you change the rootIdentifier, or add/remove a node, you have to re-run
     */
    public void run(){
        parent = new Hashtable<>();
        parent.put(rootIdentifier, NO_PARENT);
        ArrayList<Node> frontier = new ArrayList<>();
        ArrayList<Node> tempFrontier = new ArrayList<>();
        frontier.add(nodes.get(rootIdentifier));

        while(!frontier.isEmpty()) {
            for (Node current : frontier) {
                Collection<Edge> edges = current.getEdges();
                for (Edge edge: edges) {
                    Node temp = edge.getNodeConnectedTo(current);
                    if (!parent.containsKey(temp.getIdentifier())) {
                        parent.put(temp.getIdentifier(), current.getIdentifier());
                        tempFrontier.add(temp);
                    }
                }
            }
            frontier.clear();
            frontier.addAll(tempFrontier);
            tempFrontier.clear();
        }
    }

    /**
     * Set the starting point of the BFS.
     * Every time you reset your rootIdentifier, you have to re-run BFS
     * @param rootIdentifier - Root from where to start BFS
     */
    public void setRootIdentifier(Node rootIdentifier) {
        this.rootIdentifier = rootIdentifier.getIdentifier();
    }

    /**
     * Prints the shortest path from root to v
     * @param v - Node where trying to get
     */
    public void printRouteFromRootToNodeV(Node v){
        super.printRouteFromRootToNodeV(v);
    }

    /**
     * Adds an edge to the graph
     * @param edge
     */
    public void addEdge(Edge edge){
        super.addEdge(edge);
    }

    /**
     * Adds a node to the graph
     * @param node
     */
    public void addNode(Node node){
        super.addNode(node);
    }
}
