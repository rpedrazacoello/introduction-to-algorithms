package data_structures.graphs.WeightGraph;

import data_structures.graphs.Edge;
import data_structures.graphs.Node;

public final class WeightEdge extends Edge {

    int weight;

    /**
     * Constructor method of WeightEdge
     * @param node1
     * @param node2
     * @param weight
     */
    public WeightEdge(WeightNode node1, WeightNode node2, int weight) {
        super(node1, node2);
        this.weight = weight;
    }

    /**
     * Method that returns the Weight of the edge
     * @return
     */
    public int getWeight() {
        return weight;
    }

    /**
     * Method that sets the Weight of the edge
     * @param weight
     */
    public void setWeight(int weight) {
        this.weight = weight;
    }
}
