package data_structures.graphs;

public class Edge {

    private Node node1;
    private Node node2;

    /**
     * Constructor method of an Edge
     * @param node1
     * @param node2
     */
    public Edge(Node node1, Node node2) {
        this.node1 = node1;
        this.node2 = node2;
    }

    /**
     * The method receives a node X.
     * Looks if the node is connected to other node Y via the edge.
     * If it is, the method returns the node Y.
     * If X is not connected to other node via the edge, it returns null.
     * @param node
     * @return
     */
    public Node getNodeConnectedTo(Node node){
        if(node.equals(node1)){
            return node2;
        } else if (node.equals(node2)){
            return node1;
        } else {
            return null;
        }
    }

    /**
     * Returns the node1.
     * If the edge is a directed edge, then the edge goes from node1 to node2.
     * @return
     */
    public Node getNode1() {
        return node1;
    }

    /**
     * Returns the node2.
     * If the edge is a directed edge, then the edge goes from node1 to node2.
     * @return
     */
    public Node getNode2() {
        return node2;
    }
}
