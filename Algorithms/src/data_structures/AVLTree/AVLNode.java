package data_structures.AVLTree;

import util.BinaryNode;

/**
 * EXPLICATION VIDEO:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/lecture-6-avl-trees-avl-sort/
 *
 * LECTURE NOTES:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/MIT6_006F11_lec06.pdf
 */
public final class AVLNode extends BinaryNode {

    private int height;
    private int value;

    /**
     * Constructor method
     * @param value
     */
    protected AVLNode(int value) {
        this.value = value;
        this.height=0;
    }

    /**
     * Returns the value of the node
     * @return the value of the node
     */
    int getValue(){
        return this.value;
    }

    /**
     * Return the height of the node
     * @return the height of the node
     */
    int getHeight() {
        return height;
    }

    /**
     * Method that recalculates the height of the node, according to the sons.
     */
    void recalculateHeight(){
        this.height=Math.max(this.getLeftSonHeight(), this.getRightSonHeight())+1;
    }

    /**
     * Returns the Right-Son's height
     * @return Right-Son's height
     */
    int getRightSonHeight(){
        if(this.rightSon == null){
            return -1;
        }
        return ((AVLNode)this.rightSon).getHeight();
    }

    /**
     * Return the Left-Son's height
     * @return Left-Son's height
     */
    int getLeftSonHeight(){
        if(this.leftSon == null){
            return -1;
        }
        return ((AVLNode)this.leftSon).getHeight();
    }

    /**
     * Returns true if the node is a leaf, false otherwise
     * @return true if the node is a leaf, false otherwise
     */
    boolean isLeaf(){
        if(this.rightSon==null && this.leftSon==null){
            return true;
        }
        return false;
    }

    /**
     * Returns the parent of the node
     * @return parent of the node
     */
    @Override
    protected BinaryNode getParent() {
        return parent;
    }

    /**
     * Sets the Left Son of the node
     * @param leftSon
     */
    @Override
    protected void setLeftSon(BinaryNode leftSon) {
        this.leftSon = leftSon;
        if(leftSon!= null) {
            leftSon.setParent(this);
        }
    }

    /**
     * Sets the Right Son of the node
     * @param rightSon
     */
    @Override
    protected void setRightSon(BinaryNode rightSon) {
        this.rightSon = rightSon;
        if(rightSon!=null) {
            rightSon.setParent(this);
        }
    }

    /**
     * Method that swaps an oldSon for a newSon.
     * Takes care of the son-parent and the parent-son pointers.
     * @param oldSon
     * @param newSon
     */
    void swapSons(AVLNode oldSon, AVLNode newSon){
            if(this.leftSon == oldSon){
            this.leftSon=newSon;
            if(this.leftSon!=null)
                ((AVLNode)this.leftSon).setParent(this);
        } else if(this.rightSon==oldSon){
            this.rightSon=newSon;
            if(this.rightSon!=null)
                ((AVLNode)this.rightSon).setParent(this);
        }
    }

    /**
     * Method that prints in order, using (this) as a root.
     */
    void print() {
        if (this.isLeaf()) {
            System.out.println(this.value);
            return;
        }

        if (this.getLeftSon() != null) {
            ((AVLNode)this.getLeftSon()).print();
        }

        System.out.println(this.value);

        if (this.getRightSon() != null) {
            ((AVLNode)this.getRightSon()).print();
        }

    }
}
