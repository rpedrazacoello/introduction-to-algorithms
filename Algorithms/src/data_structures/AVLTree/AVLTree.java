package data_structures.AVLTree;

/**
 * EXPLICATION VIDEO:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/lecture-6-avl-trees-avl-sort/
 *
 * LECTURE NOTES:
 * https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/MIT6_006F11_lec06.pdf
 */
public class AVLTree {

    AVLNode root;

    public AVLTree(int value) {
        this.root = new AVLNode(value);
    }

    /**
     * Method that adds a new node to the tree.
     * The new node's value is @param value
     * @param value
     */
    public void insertNode(int value){
        AVLNode current = this.root;
        while(true){
            if (current.isLeaf() ||
                    (current.getLeftSon()==null && value <= current.getValue()) ||
                    (current.getRightSon()==null && value >= current.getValue())){
                AVLNode avlNode = new AVLNode(value);
                if(value <= current.getValue()){
                    current.setLeftSon(avlNode);
                } else {
                    current.setRightSon(avlNode);
                }
                fixAVLProperty(current);
                break;
            } else {
                if (value >= current.getValue()) {
                    current = (AVLNode) current.getRightSon();
                } else if (value < current.getValue()) {
                    current = (AVLNode) current.getLeftSon();
                }
            }
        }
    }

    /**
     * Method that removes a node from the tree.
     * The node's value is equal to @param value
     * @param value
     * @return True if a node was deleted, false otherwise
     */
    public boolean removeNode(int value){
        AVLNode current = this.root;
        while(true){
            if(current.isLeaf() && current.getValue()!=value){
                return false;
            }

            if (value > current.getValue()) {
                current = (AVLNode) current.getRightSon();
            } else if (value < current.getValue()) {
                current = (AVLNode)current.getLeftSon();
            } else {
                current = removeNode(current);
                fixAVLProperty(current);
                return true;
            }
        }
    }

    /**
     * Deletes a node, returns the node that took the place of the deleted node.
     * @param node
     * @return Node that took the place of the deleted node.
     */
    private AVLNode removeNode(AVLNode node){
        AVLNode temp;
        //If the deleted node is a Leaf
        if(node.isLeaf()){
            temp= (AVLNode) node.getParent();
            temp.swapSons(node, null);
        } else {
            if(node.getLeftSon()!=null) {
                temp = (AVLNode) node.getLeftSon();

                //Set relation with rightSon
                if(node.getRightSon()!= null) {
                    ((AVLNode)node.getRightSon()).setParent(temp);
                }
                temp.setRightSon(node.getRightSon());
            } else {
                temp = (AVLNode) node.getRightSon();
                node.setRightSon(null);
            }

            if(!(node==root)) {
                //temp is going to take node's place in the tree
                ((AVLNode)node.getParent()).swapSons(node, temp);
            } else {
                this.root=temp;
            }
        }

        node.setParent(null);
        node.setRightSon(null);
        node.setLeftSon(null);

        return temp;
    }

    /**
     * Method that fixes the AVL Property (binary complete tree)
     * @param current
     */
    private void fixAVLProperty(AVLNode current){
        while(true){
            current.recalculateHeight();
            if(Math.abs(current.getLeftSonHeight() - current.getRightSonHeight())>=2){
                int type = chooseRotationType(current);
                switch (type){
                    case 1: rotateType1(current); break;
                    case 2: rotateType2(current); break;
                    case 3: rotateType3(current); break;
                    case 4: rotateType4(current); break;
                }
            }
            current = (AVLNode) current.getParent();

            if (current == null) {
                //There are no more parents, you have surpased the root
                break;
            }
        }
    }

    /**
     * Righ rotation on current
     * @param current
     */
    private void rotateType1(AVLNode current){
        rightRotate(current);
    }

    /**
     * Left rotation on current
     * @param current
     */
    private void rotateType2(AVLNode current){
        leftRotate(current);
    }

    /**
     * Left Rotation on current's left son, then Right Rotation on current
     * @param current
     */
    private void rotateType3(AVLNode current){
        leftRotate((AVLNode)current.getLeftSon());
        rightRotate(current);
    }

    /**
     * Right Rotation on current's right son, then Left Rotation on current
     * @param current
     */
    private void rotateType4(AVLNode current){
        rightRotate((AVLNode)current.getRightSon());
        leftRotate(current);
    }

    /**
     * ROTATION INFORMATION: https://en.wikipedia.org/wiki/Tree_rotation
     * @param current
     */
    private void leftRotate(AVLNode current){
        AVLNode X = current;
        AVLNode Y = (AVLNode) current.getRightSon();

        AVLNode A = null;
        AVLNode B = null;
        AVLNode C = null;

        if(current.getLeftSon()!= null){
            A = (AVLNode) current.getLeftSon();
        }

        if(current.getRightSon()!=null) {
            if (((AVLNode)current.getRightSon()).getLeftSon() != null) {
                B = (AVLNode)((AVLNode)current.getRightSon()).getLeftSon();
            }
            if (((AVLNode)current.getRightSon()).getRightSon() != null) {
                C = (AVLNode)((AVLNode)current.getRightSon()).getRightSon();
            }
        }

        if(X.getParent()!=null){
            //if X is not the root
            ((AVLNode)X.getParent()).swapSons(X, Y);
        } else {
            //if X is the root then change the root
            Y.setParent(null);
            this.root=Y;
        }

        Y.setLeftSon(X);
        Y.setRightSon(C);
        X.setLeftSon(A);
        X.setRightSon(B);

        X.recalculateHeight();
        Y.recalculateHeight();
    }

    /**
     * ROTATION INFORMATION: https://en.wikipedia.org/wiki/Tree_rotation
     * @param current
     */
    private void rightRotate(AVLNode current){
        AVLNode Y = current;
        AVLNode X = (AVLNode)current.getLeftSon();

        AVLNode A = null;
        AVLNode B = null;
        AVLNode C = null;

        if(current.getLeftSon()!=null) {
            if (((AVLNode)current.getLeftSon()).getLeftSon() != null) {
                A = (AVLNode)((AVLNode)current.getLeftSon()).getLeftSon();
            }
            if (((AVLNode)current.getLeftSon()).getRightSon() != null) {
                B = (AVLNode)((AVLNode)current.getLeftSon()).getRightSon();
            }
        }

        if(current.getRightSon()!=null) {
            C = (AVLNode)current.getRightSon();
        }

        if(Y.getParent()!=null){
            //if X is not the root
            ((AVLNode)Y.getParent()).swapSons(Y, X);
        } else {
            //if X is the root then change the root
            X.setParent(null);
            this.root=X;
        }
        X.setLeftSon(A);
        X.setRightSon(Y);
        Y.setLeftSon(B);
        Y.setRightSon(C);

        X.recalculateHeight();
        Y.recalculateHeight();
    }

    /**
     * Type 1: Right rotation on current
     * Type 2: Left Rotation on current
     * Type 3: Left Rotation on current's left son, then Right Rotation on current
     * Type 4: Right Rotation on current's right son, then Left Rotation on current
     * @param current
     */
    private int chooseRotationType(AVLNode current){
        //Type 2 or Type 4
        if(current.getRightSonHeight()>current.getLeftSonHeight()){
            //Type 2
            if(((AVLNode)current.getRightSon()).getRightSonHeight()>((AVLNode)current.getRightSon()).getLeftSonHeight()){
                return 2;
            }
            //Type 4
            else {
                return 4;
            }
        }
        //Type 1 or Type 3
        else {
            //Type 1
            if(((AVLNode)current.getLeftSon()).getLeftSonHeight()>((AVLNode)current.getLeftSon()).getRightSonHeight()){
                return 1;
            }
            //Type 3
            else {
                return 3;
            }
        }
    }

    /**
     * Method that prints the sorted tree.
     */
    public void printTree(){
        root.print();
    }
}
